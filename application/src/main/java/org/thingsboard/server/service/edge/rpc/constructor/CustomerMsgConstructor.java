/**
 * Copyright © 2016-2021 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.edge.rpc.constructor;

import org.springframework.stereotype.Component;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.common.util.JacksonUtil;
import org.thingsboard.server.gen.edge.v1.CustomerUpdateMsg;
import org.thingsboard.server.gen.edge.v1.UpdateMsgType;
import org.thingsboard.server.queue.util.TbCoreComponent;

import static org.thingsboard.server.service.edge.rpc.EdgeProtoUtils.getStringValue;

@Component
@TbCoreComponent
public class CustomerMsgConstructor {

    public CustomerUpdateMsg constructCustomerUpdatedMsg(UpdateMsgType msgType, Customer customer) {
        CustomerUpdateMsg.Builder builder = CustomerUpdateMsg.newBuilder()
                .setMsgType(msgType)
                .setIdMSB(customer.getId().getId().getMostSignificantBits())
                .setIdLSB(customer.getId().getId().getLeastSignificantBits())
                .setTitle(customer.getTitle());
        if (customer.getCountry() != null) {
            builder.setCountry(getStringValue(customer.getCountry()));
        }
        if (customer.getState() != null) {
            builder.setState(getStringValue(customer.getState()));
        }
        if (customer.getCity() != null) {
            builder.setCity(getStringValue(customer.getCity()));
        }
        if (customer.getAddress() != null) {
            builder.setAddress(getStringValue(customer.getAddress()));
        }
        if (customer.getAddress2() != null) {
            builder.setAddress2(getStringValue(customer.getAddress2()));
        }
        if (customer.getZip() != null) {
            builder.setZip(getStringValue(customer.getZip()));
        }
        if (customer.getPhone() != null) {
            builder.setPhone(getStringValue(customer.getPhone()));
        }
        if (customer.getEmail() != null) {
            builder.setEmail(getStringValue(customer.getEmail()));
        }
        if (customer.getAdditionalInfo() != null) {
            builder.setAdditionalInfo(getStringValue(JacksonUtil.toString(customer.getAdditionalInfo())));
        }
        return builder.build();
    }

    public CustomerUpdateMsg constructCustomerDeleteMsg(CustomerId customerId) {
        return CustomerUpdateMsg.newBuilder()
                .setMsgType(UpdateMsgType.ENTITY_DELETED_RPC_MESSAGE)
                .setIdMSB(customerId.getId().getMostSignificantBits())
                .setIdLSB(customerId.getId().getLeastSignificantBits()).build();
    }
}
